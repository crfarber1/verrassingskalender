import React from 'react';
import { Container } from './styling';
import Grid from '../../components/Grid/Grid';
import useReset from '../../hooks/useReset';

const GamePage: React.FC = () =>  {
    useReset();
    return(
        <Container>
            <Grid />
        </Container>
    )
}

export default GamePage;