import styled from 'styled-components';
import theme from '../../theme';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;
`

export const Content = styled.div`
    margin: 10px;
    padding: 20px 20px 40px;
    max-width: 500px;
    background: linear-gradient(-182deg, 
        ${theme.colors.brand.primary} 0%, 
        ${theme.colors.brand.primary} 96%, 
        transparent 96%, 
        transparent 100%
    );
`

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    grid-gap: 10px;
`