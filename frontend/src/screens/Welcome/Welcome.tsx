import React from 'react';
import { Container, Content, Form } from './styling';
import useReset from '../../hooks/useReset';
import Button from '../../components/Button/Button';
import { ButtonColors } from '../../components/Button/types';
import InputField from '../../components/InputField/InputField';
import useParticipants from '../../hooks/useParticipants';

const WelcomePage: React.FC = () => {
    const { values, handlers, error } = useParticipants();
    useReset();

    return (
        <Container>
            <Content>
                <h1>Welkom bij het nieuwe online kansspel van NLO!</h1>
                <p>
                Ontdek onze spannende verrassingskalender met <strong>één kans </strong>
                om te krassen en maak kans op de spectaculaire hoofdprijs van <strong>25.000 euro </strong>. 
                Maar dat is nog niet alles - er zijn ook 100 troostprijzen van <strong>100 euro </strong> 
                te winnen! Kras en win vandaag nog en word misschien wel de volgende grote winnaar van NLO. Waar wacht je nog op?
                </p>
                <h3>Speel mee en beleef de spanning!</h3>
                <Form onSubmit={handlers.save}>
                    <InputField label={"Hoe heet je?"} name={"name"} placeholder={"Vul in je naam"} value={values.name} onChange={handlers.name} error={error} tabIndex={1} />
                    <Button text={"Doe mee!"} color={ButtonColors.primary} buttonType={"submit"} tabIndex={2} />
                </Form>
            </Content>
        </Container>
    )
}

export default WelcomePage;

