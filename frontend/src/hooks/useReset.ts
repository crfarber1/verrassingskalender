import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

// custom hook to handle the reset logic and state management
// this hook will listen to the ctrl + enter key combination
// and will clear the localstorage and reload the page
// this is a handy way to reset the game
const useReset = () => {
    const navigate = useNavigate();
    useEffect(() => {
        const handleKeyDown = (event: KeyboardEvent) => {
            if (event.ctrlKey && event.key === 'Enter') {
                localStorage.clear();
                window.location.reload();
                return navigate('/')
            }
        }
        window.addEventListener('keydown', handleKeyDown);
        return () => {
            window.removeEventListener('keydown', handleKeyDown);
        };
    }, [navigate]);

    return {};
}

export default useReset;