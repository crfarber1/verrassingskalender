import { useEffect, useState } from 'react';
import { PriceCell } from '../components/Grid/types';
import { localstorageParticipantKey } from './useParticipants';
import { generateRandomizedPriceGrid } from '../components/Grid/utils';
import { Participant } from '../types/participants';

export const localstorageGameKey = 'game_data';

// Custom hook to handle game logic and state management
export const useGame = () => {
    const [isLoaded, setIsLoaded] = useState(false);
    const [priceCells, setPriceCells] = useState<PriceCell[] | null>(null);
    const [participant, setParticipant] = useState<Participant | null>(null);

    // Load participant from localstorage if it exists on page load
    useEffect(() => {
        const storedUserChoice = localStorage.getItem(localstorageParticipantKey);
        if (storedUserChoice) {
            setParticipant(JSON.parse(storedUserChoice));
        }
    }, []);

    // Load price cells from localstorage or generate new ones on page load
    useEffect(() => {
        const storedData = localStorage.getItem(localstorageGameKey);
        // Load from localstorage if it exists
        if (storedData) {
            setPriceCells(JSON.parse(storedData));
            setIsLoaded(true);
            return;
        }
        // Generate new price cells
        const initialGrid = generateRandomizedPriceGrid();
        setPriceCells(initialGrid);
        // Save to localstorage
        localStorage.setItem(localstorageGameKey, JSON.stringify(initialGrid));
        setIsLoaded(true);
    }, []);

    // Save participant to localstorage when it changes
    useEffect(() => {
        // Save to localstorage if it exists and has a price
        if (participant) {
            localStorage.setItem(localstorageParticipantKey, JSON.stringify(participant));
        }
    }, [participant]);

    // Handle choosing a cell by updating the price cell and participant
    const handleChooseCell = (cellIndex: number) => {
        // If the participant already has a price, don't do anything 
        // or if the price cells haven't loaded yet
        if (participant?.price !== null || !priceCells) {
            return;
        }

        // If the cell is already active, don't do anything
        const selectedCell = priceCells[cellIndex];
        if (!selectedCell.isActive) {
            // Update the price cell to be active and save to localstorage
            const updatedPriceCells = [...priceCells];
            updatedPriceCells[cellIndex] = { ...selectedCell, isActive: true };
            setPriceCells(updatedPriceCells);
            localStorage.setItem(localstorageGameKey, JSON.stringify(updatedPriceCells));

            // Update the participant with the price
            setParticipant({
                ...participant,
                price: selectedCell.price,
                choice: cellIndex,
            });
        }
    };

    return {
        isLoaded,
        priceCells,
        handleChooseCell,
        participant,
    };
};
