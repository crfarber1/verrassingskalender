import { useState, useEffect, ChangeEvent, FormEvent } from "react";
import { Participant } from "../types/participants";
import { useNavigate } from "react-router-dom";

export const localstorageParticipantKey = 'participant';

// custom hook to handle the participants logic and state management
const useParticipants = () => {
    const navigate = useNavigate();
    const [name, setName] = useState<string>('');
    const [participant, setParticipant] = useState<Participant | null>(() => {
        const storedUserChoice = localStorage.getItem(localstorageParticipantKey);
        return storedUserChoice ? JSON.parse(storedUserChoice) : null;
    });
    const [error, setError] = useState<string>('');

    useEffect(() => {
        // check if there is a participant in localstorage
        const participant = localStorage.getItem('participant');
        if (participant) {
            setParticipant(JSON.parse(participant));
        }
    }, []);

    // handle the name input change
    const handleName = (event: ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        setName(event.target.value);
    }

    // handle the register form submit
    const handleRegister = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (!name) return setError('Sorry, je moet een naam invullen');
        // create a new participant object
        const newParticipant = { name, price: null, choice: null };
        // store the user choice in localstorage
        localStorage.setItem('participant', JSON.stringify(newParticipant));
        // set the participant state
        setParticipant(newParticipant);
        return navigate('/game');
    }

    // return form and participant values
    const values = {
        name,
        participant
    }

    // return form handlers
    const handlers = {
        name: handleName,
        save: handleRegister,
    }

    return { values, handlers, error };
}

export default useParticipants;