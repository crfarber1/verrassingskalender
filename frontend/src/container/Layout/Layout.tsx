import React from 'react';
import LayoutProps from './types';
import { Container } from './styling';

const Layout: React.FC<LayoutProps> = ({ children }) => (
    <Container>
        {children}
    </Container>
)

export default Layout;