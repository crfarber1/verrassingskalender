// Here you can find all settings for the game
// You can change the settings to your liking
//  - options: the amount of options to choose from
//  - preset: the amount of preset options to simulate other players
//  - prices: the prices for the main and second price
//  - available: the amount of prices available in that price range
// -  price: value of the price

export const gameSetting = {
    options: 10000,
    preset: 10,
    prices: {
        main: {
            available: 1,
            price: 25000,
        },
        second: {
            available: 100,
            price: 100,
        }
    }
}