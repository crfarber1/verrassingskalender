import { useNavigate } from "react-router-dom";
import useParticipants from "../hooks/useParticipants";
import { useEffect } from "react";

// This is a custom hook that will redirect the user to the login page if they are not authenticated
export const useAuthCheck = (): boolean => {
  const navigate = useNavigate();
  const { values } = useParticipants();

  // If the user is not authenticated, redirect to the login page
  useEffect(() => {
    if (!values.participant) {
      navigate('/');
    }
  }, [navigate, values.participant]);

  // Convert the participant object to a boolean
  return Boolean(values.participant);
}
