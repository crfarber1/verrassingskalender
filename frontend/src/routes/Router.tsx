import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import WelcomePage from '../screens/Welcome/Welcome';
import GamePage from '../screens/Game/GameScreen';
import { ProtectedRoute } from './ProtectedRoute';

const AppRouter: React.FC = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<WelcomePage />} />
                <Route path="/game"
                    element={<ProtectedRoute><GamePage /></ProtectedRoute>}
                />
            </Routes>
        </BrowserRouter>
    );
}

export default AppRouter;
