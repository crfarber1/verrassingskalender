import React, { Fragment, ReactElement } from "react";
import { Navigate } from "react-router-dom";
import { useAuthCheck } from "./useAuthCheck";
import { ProtectedRouteProps } from "./types";

// This is a wrapper around the Route component that will redirect the user to the login page if they are not authenticated
export const ProtectedRoute: React.FC<ProtectedRouteProps> = ({ children }): ReactElement => {
    const canPlay = useAuthCheck();

    if (!canPlay) {
        return <Navigate to={"/"} replace />
    }

    return <Fragment>{children}</Fragment>;
};