export interface Participant {
    name: string;
    price: number | null;
    choice: number | null;
}