const theme = {
    colors: {
        white: '#fff',
        black: '#000',
        highlight: '#00A6FF',
        brand: {
            primary: '#D6EB77',
        }
    }
}

export default theme