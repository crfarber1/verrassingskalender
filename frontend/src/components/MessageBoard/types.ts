import { Participant } from "../../types/participants";

export interface MessageBoardProps {
    participant: Participant | null;
}