import { gameSetting } from "../../data/settings";
import { Participant } from "../../types/participants";

// Get the game information based on the participant
// This is used to generate the message for the participant
export const getGameInformation = (participant: Participant | null) => {
    const price = participant?.price;

    if (price === undefined) {
        return `Geen participant gevonden!`;
    }

    // if the price is null the participant has not scratched a cell
    if (price === null) {
        return `Hallo ${participant?.name}! Laten we eens kijken of je vandaag een prijs zult winnen!`;
    }

    // if the price is 0 the participant has not won a price
    if (price === 0) {
        return `Sorry ${participant?.name}! Je hebt deze keer geen prijs gewonnen!`;
    }

    // if the value of the price is the same as the main or second price the participant has won a price
    if (price === gameSetting.prices.main.price || price === gameSetting.prices.second.price) {
        return `Gefeliciteerd ${participant?.name}! Je hebt ${getStyledMessagePrice(price)}€ gewonnen!`;
    }
}

// Styling the message price based on the value add a dot every 3 numbers
// 1000 => 1.000 
export const getStyledMessagePrice = (number: number): string => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}
