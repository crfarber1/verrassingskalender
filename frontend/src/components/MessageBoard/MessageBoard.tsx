import React from "react";
import { useNavigate } from "react-router-dom";
import Button from "../Button/Button";
import { getGameInformation } from "./utils";
import { Container } from "./styling";
import { ButtonColors } from "../Button/types";
import { MessageBoardProps } from "./types";

const MessageBoard: React.FC<MessageBoardProps> = ({ participant }) => {
    const navigate = useNavigate();
    return (
        <Container>
            <h3>{getGameInformation(participant)}</h3>
            <Button onClick={() => navigate('/')} color={ButtonColors.secondary} tabIndex={0} text='Speel nog een keer!' />
        </Container>
    )
}

export default MessageBoard;