import styled from 'styled-components';
import theme from '../../theme';

export const Container = styled.div`
  margin: 20px;
  padding: 20px;

  display: grid;
  grid-template-columns: 1fr auto;
  align-items: center;
  grid-gap: 10px;
  
  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }

  border-radius: 10px;
  background: ${theme.colors.brand.primary};

  position: fixed;
  left: 0;
  bottom: 0;
  z-index: 999;
`