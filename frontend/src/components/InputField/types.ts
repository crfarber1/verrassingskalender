import { ChangeEvent } from "react";

export interface InputFieldProps {
    label: string;
    name: string;
    value: string;
    placeholder: string;
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
    error: string;
    tabIndex: number;
}

