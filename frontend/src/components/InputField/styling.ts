import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`;

export const Label = styled.label`
    margin-bottom: 4px;
    font-size: 12px;
    font-weight: bold;
`;

export const Input = styled.input`
    padding: 10px;

    border-radius: 5px;
    border: 0;

    box-shadow: 0 0 5px 0 rgba(0,0,0,0.2);
`

export const ErrorBanner = styled.div`
    margin: 4px 0;
    padding: 2px 10px;

    background-color: #fff;
    color: red;
    border: 1px solid rgba(255, 255,255,0.1);
    border-radius: 5px;
    box-shadow: 0 0 5px 0 rgba(0,0,0,0.08);

    font-size: 12px;
`