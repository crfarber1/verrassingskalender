import React from "react";
import { Container, Label, Input, ErrorBanner } from "./styling";
import { InputFieldProps } from "./types";


const InputField: React.FC<InputFieldProps> = ({ label, name, value, placeholder, onChange, error, tabIndex }) => (
    <Container>
        <Label htmlFor={name}>{label}</Label>
        <Input name={name} type="text" placeholder={placeholder} value={value} onChange={onChange} tabIndex={tabIndex} />
        {error && <ErrorBanner><span>{error}</span></ErrorBanner>}
    </Container>
)
export default InputField

