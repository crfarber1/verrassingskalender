import { ButtonColors, ColorScheme } from "./types";

// define the button colors
// i used a record type to define the buttonColors object
// and colorScheme type to define the return values of the record
const buttonColors: Record<ButtonColors, ColorScheme> = {
    primary: {
        bgColor: '#000',
        hoverColor: '#333',
        focusColor: '#111',
        brColor: '#000',

        textColor: '#fff',
    },
    secondary: {
        bgColor: 'transparent',
        hoverColor: 'rgba(0,0,0, .1)',
        focusColor: 'rgba(255,255,255, .1)',
        brColor: '#000',

        textColor: '#000',
    }
};

export default buttonColors;