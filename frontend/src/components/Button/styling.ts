import styled from "styled-components";
import { ButtonContainerProps, ButtonTextProps } from "./types";
import buttonColors from "./colors";

export const Container = styled.button<ButtonContainerProps>(({ color }) => {
    
    // get the colors from the buttonColors object
    const bgColor = buttonColors[color].bgColor
    const hoverColor = buttonColors[color].hoverColor
    const focusColor = buttonColors[color].focusColor
    const brColor = buttonColors[color].brColor

    return `
    padding: 4px 8px;
    height: 44px;
    min-width: 80px;
    display: flex;
    align-items: center;
    justify-content: center;

    border-radius: 5px;

    border: 2px solid ${brColor};
    background-color: ${bgColor};

    cursor: pointer;

    &:hover{
        background-color: ${hoverColor};
    }

    &:focus{
        background-color: ${focusColor};
    }
`})

export const Text = styled.span<ButtonTextProps>(({ color }) => {
    const textColor = buttonColors[color].textColor

    return `
        font-weight: 600;
        color: ${textColor};
    `
})