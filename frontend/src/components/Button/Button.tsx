
import React from 'react';
import { ButtonProps } from './types';
import { Container, Text } from './styling';

const Button: React.FC<ButtonProps> = ({ color, onClick, buttonType, text, tabIndex }) => (
    <Container type={buttonType} color={color} onClick={onClick} tabIndex={tabIndex}>
        <Text color={color}>{text}</Text>
    </Container>
)

export default Button;