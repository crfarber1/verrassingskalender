export interface ButtonProps{
    text: string;
    color: ButtonColors;
    buttonType?: "button" | "submit" | "reset";
    onClick?: () => void;
    tabIndex: number;
}

export enum ButtonColors {
    primary = "primary",
    secondary = "secondary"
}

export interface ColorScheme {
    bgColor: string;
    hoverColor: string;
    focusColor: string;
    
    brColor: string;

    textColor: string;
}

export interface ButtonContainerProps {
    color: ButtonColors;
}

export interface ButtonTextProps {
    color: ButtonColors;
}