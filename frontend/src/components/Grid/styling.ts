import styled from 'styled-components';

export const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const GridContainer = styled.div`
  padding: 0 0 200px;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  gap: 5px;
`;