export interface PriceCell {
    price: number;
    isActive: boolean;
}