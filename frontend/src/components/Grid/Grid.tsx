import React from 'react';
import { Container, GridContainer } from './styling';
import ScratchBox from '../ScratchBox/ScratchBox';
import { useGame } from '../../hooks/useGame';
import MessageBoard from '../MessageBoard/MessageBoard';

const Grid: React.FC = () => {
    const { isLoaded, participant, priceCells, handleChooseCell } = useGame();

    if (!isLoaded || !priceCells) return <Container><span>...loading</span></Container>

    return (
        <Container>
            <MessageBoard participant={participant} />
            <GridContainer>
                {priceCells.map((cell, index) => (
                    <ScratchBox
                        key={`cell-${index}`}
                        price={cell.price}
                        isActive={cell.isActive}
                        userHasChosen={participant?.choice === index}
                        onClick={() => handleChooseCell(index)}
                    />
                ))}
            </GridContainer>
        </Container>
    );
};

export default Grid;

