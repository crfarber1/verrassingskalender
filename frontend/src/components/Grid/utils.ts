import { gameSetting } from "../../data/settings";
import { PriceCell } from "./types";

// Generate a randomized price grid
export const generateRandomizedPriceGrid = () => {
    // create an array with the length of the options
    // fill it with objects with the price and isActive properties
    // price: the value of the price
    // isActive: if the cell is scratched
    const initialGrid: PriceCell[] = new Array(gameSetting.options).fill({
        price: 0,
        isActive: false,
      }); 

    // track the amount of prices added to the grid
    // this is used to check if the amount of prices added is not more than the available prices
    // this is done to prevent adding more prices than available

    let trackSmallPrices = 0;
    let trackBigPrice = 0;

    // get the available prices from the gameSetting
    const bigPrice = gameSetting.prices.main;
    const smallPrice = gameSetting.prices.second;

    // loop over the initialGrid and add the prices to the grid
    const newCells = initialGrid.map(cell => {
        // add the big price to the grid
        // if the random number is lower than 0.1 and the trackBigPrice is lower than the available prices
        if (Math.random() < 0.1 && trackBigPrice++ < bigPrice.available) {
            return { ...cell, price: bigPrice.price };
        }
        // same as above but for the small price
        if (Math.random() < 0.4 && trackSmallPrices++ < smallPrice.available) {
            return { ...cell, price: smallPrice.price };
        }
        return cell;
    });

    // create a array with uniek numbers using a Set   
    // fill it with random numbers between 0 and the amount of options
    const activeCellsPresets = new Set();
    while (activeCellsPresets.size < gameSetting.preset) {
        activeCellsPresets.add(Math.floor(Math.random() * gameSetting.options));
    }
    // loop over the newCells and set the isActive property to true if the index is in the activeCellsPresets array
    // this is done to simulate other players
    const gridWithPresetCells = newCells.map((cell, index) => ({
        ...cell,
        isActive: activeCellsPresets.has(index),
    }));

    // return the grid with the preset cells randomized
    // this is done to prevent the preset cells to be in the same order every time
    return gridWithPresetCells.sort(() => Math.random() - 0.5)
};