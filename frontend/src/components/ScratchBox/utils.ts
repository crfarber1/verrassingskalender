import { gameSetting } from "../../data/settings";

// Styling the cell price based on the value
export const getStyldCellPrice = (value: number | null, isActive: boolean): string => {
    // if the cell is scratched
    if (!isActive) return 'Niet gekrast'

    // if the cell is scratched and has a price
    if (value === gameSetting.prices.second.price) return '💵'
    if (value === gameSetting.prices.main.price) return '💰'

    // if there is no price
    return '✖️'
};

