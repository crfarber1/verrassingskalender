export interface ScratchBoxProps {
    price: number | null;
    isActive: boolean;
    userHasChosen?: boolean;
    onClick: () => void;
}

export interface ContainerStylingProps {
    isActive: boolean;
    userHasChosen?: boolean;
}

export interface ContentStylingProps {
    isActive?: boolean;
}

export interface OverlayStylingProps {
    isActive: boolean;
}