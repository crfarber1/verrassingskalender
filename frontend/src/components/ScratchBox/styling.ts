import styled from 'styled-components';
import { ContainerStylingProps, ContentStylingProps } from './types';
import theme from '../../theme';

export const Container = styled.div<ContainerStylingProps>(({ isActive, userHasChosen }) => {
  const opacityWhenHover = isActive ? 1 : .7;
  const hasChosen = userHasChosen ? `0 0 0 2px ${theme.colors.highlight}` : '0 0 0 1px #EDC967';
  const background = isActive ?
    `${theme.colors.white}` :
    'linear-gradient(30deg, #AE8625, #F7EF8A, #D2AC47, #EDC967)';

  return `
    width: 20px;
    height: 20px;
    background: ${background};
    position: relative;
    border-radius: 5px;
  box-shadow: inset ${hasChosen};
    cursor: pointer;

    &:hover {
      opacity: ${opacityWhenHover};
      transition: opacity 0.2s ease-in-out;
    }
  `;
});

export const Content = styled.div<ContentStylingProps>(({ isActive }) => {
  const opacity = isActive ? 1 : 0;
  return `
    height: 100%;
    width: 100%;

    color: #000;
    font-size: 12px;
    font-weight: 600;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    opacity: ${opacity};
    transition: opacity 1.5s ease-in-out;
`});
