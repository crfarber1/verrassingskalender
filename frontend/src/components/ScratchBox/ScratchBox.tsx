import React from 'react';
import { ScratchBoxProps } from './types';
import { Container, Content } from './styling';
import { getStyldCellPrice } from './utils';

const ScratchBox: React.FC<ScratchBoxProps> = ({ price, isActive, userHasChosen, onClick }) => (
  <Container isActive={isActive} onClick={onClick} userHasChosen={userHasChosen}>
    <Content isActive={isActive}>{getStyldCellPrice(price, isActive)}</Content>
  </Container>
)

export default ScratchBox;
