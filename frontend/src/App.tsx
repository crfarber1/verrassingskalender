import React from 'react';
import AppRouter from './routes/Router';
import Layout from './container/Layout/Layout';

const App = () => <Layout><AppRouter /></Layout>

export default App;