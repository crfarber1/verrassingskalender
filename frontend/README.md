# Getting Started with Create React App
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Folder Structure

- `frontend`: contains the app
- `node_modules/`: contains all of the project's dependencies
- `public/`: contains the HTML template and other static assets that are copied directly to the build folder
  - `index.html`: the HTML template for the app
  - `favicon.ico`: the app's favicon
  - `manifest.json`: a web app manifest file
- `src/`: contains the source code for the app
  - `assets/`: general app assets
    - `fonts/`: app fonts
  - `components/`: contains reusable components used by multiple pages
    - `[component]/styling.js`: contains styling only used for that compoment
    - `[component]/utils.js`: contains helpers only used for that compoment
    - `[component]/types.js`: contains types only used for that compoment
  - `container/`: componant wrappers or layouts
  - `data/`: local app data and settings
  - `hooks/`: contains hooks and localstate of the app
  - `routes/`: contains the pages of the app
  - `types/`: contains shared types that are used in functions, components etc...

## Available Scripts
In the project directory, you can run:

### `yarn start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.